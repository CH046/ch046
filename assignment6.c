#include <stdio.h>
void calc(int);
int main() 
{ 
    int k;
    printf("Enter an integer: ");
    scanf("%d",&k);
    calc(k);
	return 0;
}

void calc(int n)
{
     int reversedN=0,remain,originalN;
    
    originalN=n;
    while(n!=0) 
	{
        remain=n%10;
        reversedN=reversedN*10+remain;
        n/=10;
    }
    if(originalN==reversedN)
    printf("%d is a palindrome.",originalN);
    else
    printf("%d is not a palindrome.",originalN);
}