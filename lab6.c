#include<stdio.h>
void find(int);
int main()
{   int g;
    printf("Program to interchange elements in an array\n");
    printf("How many entries ?\n");
    scanf("%d",&g);
    find(g);
    return 0;
}
void find(int n)
{
   int a[100],i,s,b,loc_s,loc_b;   
   printf("Enter the numbers one below another.\n") ;
   for(i=0;i<n;i++)
   {
    scanf("%d",&a[i]);
   }
   printf("The array before interchanging the numbers.\n");
    
    for(i=0;i<n;i++)
   {
    printf("%d\t",a[i]);
   }
   s=a[0];b=a[1];
   for(i=0;i<n;i++)
   {
      if(s>a[i])
      {
          s=a[i];
          loc_s=i;
      }
      if(b<a[i])
      {
          b=a[i];
          loc_b=i;
      }
    }
    a[loc_s]=b;
    a[loc_b]=s;
    
    printf("\nThe array after interchanging the numbers.\n");
    
    for(i=0;i<n;i++)
   {
    printf("%d\t",a[i]);
   }
}
