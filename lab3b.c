#include<stdio.h>
#include<math.h>
void findRoots(int,int,int);
int main() 
{ 
    int x,y,z;
    printf("Enter the coefficients of the quadratic equation x, y and z :  ");
    scanf("%d%d%d",&x,&y,&z);
    findRoots(x,y,z); 
	return 0;
}

void findRoots(int a,int b,int c) 
{
   
  
    int d = (b*b)-(4*a*c); 
    double sqrt_val = sqrt(d); 
     
  
    if (d>0) 
    { 
    double r1=(-b+(sqrt_val))/2*a;
    double r2=(-b-(sqrt_val))/2*a;
        printf("Roots are real and different \n"); 
        printf("%f\n%f",r1,r2); 
    } 
    else if (d==0) 
    {  double s1=-b/(2*a);
        printf("Roots are real and same \n"); 
        printf("%f",s1);
    } 
    else
    { 
        printf("Roots are complex "); 
         
    } 
} 

