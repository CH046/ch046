#include<stdio.h>
void perform();
void perform(int *a,int *b)
{
    int m;
    m=*a;
    *a=*b;
    *b=m;
}
int main()
{
    printf("Program to swap two values using call by reference.\n");
    int num1,num2;
    printf("Enter value of num 1\n");
    scanf("%d",&num1);
    printf("Enter value of num 2\n");
    scanf("%d",&num2);
    printf("Before Swapping, num1 is: %d, num2 is: %d\n",num1,num2);
    perform(&num1,&num2);
    printf("After  Swapping, num1 is: %d, num2 is: %d\n",num1,num2);
    return 0;
}


