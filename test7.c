#include <stdio.h>
#include<string.h>
void perform();
int main()
{
    printf("Program to  display name of the student with highest score using structures.\n");
    perform();
    return 0;
}
void perform()
{
  struct student
  {  
     int r_no;  
     char name[20]; 
     char course[20]; 
     float fees; 
     int marks;
     char DOB[10];
      
  };
  struct student stud1;
  struct student stud2;
  //codes to recieve data
  printf("Enter the roll number of the student 1 \n");    
  scanf("%d",&stud1.r_no);    
  printf("Enter the name of the student 1 \n");   
  scanf("%s",stud1.name);    
  printf("Enter the course of the student 1 \n");  
  scanf("%s",stud1.course);    
  printf("Enter the fees of the student 1 \n");    
  scanf("%f",&stud1.fees); 
  printf("Enter the marks scored by the student 1\n");
  scanf("%d",&stud1.marks);
  printf("Enter the DOB of the student  1 \n"); 
  scanf("%s",stud1.DOB);  
  printf("Enter the roll number of the student 2 \n");    
  scanf("%d",&stud2.r_no);    
  printf("Enter the name of the student 2 \n");   
  scanf("%s",stud2.name);    
  printf("Enter the course of the  student 2 \n");  
  scanf("%s",stud2.course);    
  printf("Enter the fees of the  student 2 \n");    
  scanf("%f",&stud2.fees); 
  printf("Enter the marks scored by the student 2\n");
  scanf("%d",&stud2.marks);
  printf("Enter the DOB of the  student 2 \n"); 
  scanf("%s",stud2.DOB);  
 //codes to output data
  printf("Student 1 data\n");
  printf("Roll Num = %d\n",stud1.r_no);    
  printf("Name = %s\n",stud1.name);  
  printf("Course = %s\n",stud1.course);    
  printf("Fees = %f\n",stud1.fees);
  printf("Marks = %d\n",stud1.marks);
  printf("DOB = %s\n",stud1.DOB);
  printf("Student 2 data\n");
  printf("Roll Num= %d\n",stud2.r_no);    
  printf("Name = %s\n",stud2.name);  
  printf("Course = %s\n",stud2.course);    
  printf("Fees = %f\n",stud2.fees);
  printf("Marks = %d\n",stud2.marks);
  printf("DOB= %s\n",stud2.DOB); 
  //code to find highest score
  if(stud1.marks>stud2.marks)
  {
      printf("%s got higher marks.\n",stud1.name);
  }
   if(stud1.marks<stud2.marks)
  {
      printf("%s got higher marks.\n",stud2.name);
  }
  if(stud1.marks==stud2.marks)
  {
      printf("Both the students scored equal marks.");
  }
}

