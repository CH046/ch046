#include<stdio.h>
void perform();
int main()
{
  printf("Program to perform arithmetic operations using pointers.\n");
  perform();
  return 0;
}
void perform()
{
   int first,second,*p,*q,sum,diff,mul,divi,rem;
   printf("Enter two integers\n");
   scanf("%d%d",&first,&second);
   p=&first;
   q=&second;
   sum=*p+*q;
   diff=*p-*q;
   mul=(*p)*(*q);
   divi=(*p)/(*q);
   rem=(*p)%(*q);
   printf("Sum of the numbers = %d\n",sum);
   printf("diff of the numbers = %d\n",diff);
   printf("product  of the numbers = %d\n",mul);
   printf(" division of the numbers = %d\n",divi);
   printf("reminder  of the numbers = %d\n",rem);
}
